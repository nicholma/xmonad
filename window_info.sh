desktop=$(wmctrl -d | grep "*" | cut -d' ' -f1)
active_window=$(printf 0x%08x $(xdotool getactivewindow))

windows=$(wmctrl -l |  tr -s ' ' |awk -v desktop="${desktop}" '$2==desktop { print $0 }' | sort)
window_count=$(echo "${windows}" | wc -l)

index=$(echo "${windows}" | awk -v active_window="${active_window}" '$1==active_window { print NR }')

if [[ "${window_count}" -gt 1 ]]; then
echo " ${index}/${window_count}"
fi

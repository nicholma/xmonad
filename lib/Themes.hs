{-# OPTIONS_GHC -fwarn-unused-imports #-}
module Themes ( titleTheme
              , xpConfig
              , gsConfig
              , colorizer
              ) where

import XMonad

import XMonad.Actions.GridSelect
import XMonad.Layout.Tabbed
import XMonad.Prompt

import qualified Config as C

-- Title theme
titleTheme :: Theme
titleTheme = def
  { fontName            = C.smallFont
  , inactiveBorderColor = C.color "blue alt"
  , inactiveColor       = C.color "blue alt"
  , inactiveTextColor   = C.color "blue alt"
  , activeBorderColor   = C.color "blue"
  , activeColor         = C.color "blue"
  , activeTextColor     = C.color "blue"
  , urgentBorderColor   = C.color "yellow"
  , urgentTextColor     = C.color "yellow"
  , decoHeight          = 6
  }

-- Prompt theme
xpConfig :: XPConfig
xpConfig = def
  { font                = C.fixedFont
  , bgColor             = C.color "blue"
  , fgColor             = C.color "white"
  , bgHLight            = C.color "blue"
  , fgHLight            = C.color "white alt"
  , borderColor         = C.color "blue"
  , promptBorderWidth   = 0
  , height              = 24
  , position            = Bottom
  , historySize         = 100
  , historyFilter       = deleteConsecutive
  , autoComplete        = Nothing
  }


-- GridSelect color scheme
colorizer :: Window -> Bool -> X (String, String)
colorizer = colorRangeFromClassName
  (0x97,0xe8,0xb4) -- lowest inactive bg
  (0x74,0xe0,0xae) -- highest inactive bg
  (0x33,0x33,0x33) -- active bg
  (0x33,0x33,0x33) -- inactive fg
  (0x44,0xAA,0xCC) -- active fg

-- GridSelect theme
gsConfig :: t -> GSConfig Window
gsConfig _ = (buildDefaultGSConfig colorizer)
  { gs_cellheight  = 50
  , gs_cellwidth   = 200
  , gs_cellpadding = 10
  , gs_font        = C.smallFont
  }

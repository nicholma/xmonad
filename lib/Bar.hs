{-# OPTIONS_GHC -fwarn-unused-imports -fno-warn-unused-top-binds #-}
module Bar ( lemonSpawnPipe
           , lemonFlags
           , lemonLogHook
           ) where

import XMonad
-- import XMonad.StackSet
import XMonad.Hooks.DynamicLog
import XMonad.Util.Loggers
import XMonad.Util.Run
import XMonad.Util.Scratchpad
-- import qualified XMonad.StackSet as W

import Data.List
import System.IO (Handle, hPutStrLn)
import Control.Applicative
import Control.Exception as E

import Utils
import qualified Config as C

-- Launch dzen through the system shell and return a Handle to its standard input
lemonSpawnPipe lf = spawnPipe $ "lemonbar" ++ lemonFlagsToStr lf ++ " | bash"

lemonFlags :: Res -> LFlag
lemonFlags r = LFlag
  { xPosLF       = 12
  , yPosLF       = 6
  , widthLF      = xRes r - 24
  , heightLF     = 24
  , clickableLF  = 20
  , fgColorLF    = C.color "white alt"
  , bgColorLF    = C.color "bar"
  , fontsLF      = lemonFonts
  , optionLF     = "-d"
  }

lemonLogHook :: Handle -> X ()
lemonLogHook h = dynamicLogWithPP $ def
  { ppOutput          = hPutStrLn h
  , ppSort            = (. scratchpadFilterOutWorkspace) <$> ppSort def
  , ppOrder           = \(ws:_:_:x) -> ws : x
  , ppSep             = " "
  , ppWsSep           = ""
  , ppCurrent         = lemonPadLeft 6 . lemonPadAround 2 . lemonColorWS (C.color "blue") "●"
  , ppUrgent          = lemonPadLeft 6 . clickWorkspace (C.color "red") "●"
  , ppVisible         = lemonPadLeft 6 . clickWorkspace (C.color "cyan alt") "●"
  , ppHiddenNoWindows = lemonPadLeft 6 . clickWorkspace (C.color "white alt") "○"
  , ppHidden          = lemonPadLeft 6 . clickWorkspace (C.color "white alt") "●"
  , ppExtras          = [myWindowsL, myFocusL, myBatL, myDateL]
  } where
  clickWorkspace color char ws =
    lemonClick 1 (xdo "w;" ++ xdo index) $ -- Mouse 1
    lemonClick 3 (xdo "w;" ++ xdo index) $ -- Mouse 3
    lemonPadAround 2 $                     -- Padding click area
    lemonColor color char where
       wsIdxToString Nothing = "1"
       wsIdxToString (Just n) = show $ mod (n+1) $ length C.myWorkspaces
       index = wsIdxToString (elemIndex ws C.myWorkspaces)
       xdo key = "/usr/bin/xdotool key super+" ++ key

--------------------------------------------------------------------------------------------
-- Helpers                                                                                --
--------------------------------------------------------------------------------------------

lemonFonts :: [String]
lemonFonts = ["Work Sans-9"]

lemonRightAlign :: String -> String
lemonRightAlign text = "%{r}" ++ text

lemonCentreAlign :: String -> String
lemonCentreAlign text = "%{c}" ++ text

lemonLeftAlign :: String -> String
lemonLeftAlign text = "%{l}" ++ text

lemonPadLeft :: Int -> String -> String
lemonPadLeft num text= "%{O" ++ show num ++ "}" ++ text

lemonPadAround :: Int -> String -> String
lemonPadAround num text= "%{O" ++ show num ++ "}" ++ text ++ "%{O" ++ show num ++ "}"

lemonClick :: Int -> String -> String -> String
lemonClick button action text = "%{A" ++ show button ++ ":" ++ action ++ ":}" ++ text ++ "%{A}"

lemonColorWS :: String -> String -> WorkspaceId -> String
lemonColorWS color text _ = "%{F" ++ color ++ "}" ++ text

lemonColor :: String -> String -> String
lemonColor color text = "%{F" ++ color ++ "}" ++ text

lemonFont :: Int -> String -> String
lemonFont num text = "%{T" ++ show num ++ "}" ++ text ++ "%{T-}"



--------------------------------------------------------------------------------------------
-- LOGGERS CONFIG                                                                         --
--------------------------------------------------------------------------------------------

-- BotRight Loggers
myBatL =
  lemonRightAlignL $
  lemonPadLeftL 20 $
  labelL (lemonColor (C.color "white alt") "•") ++!
  lemonPadAroundL 10 (lemonColorL (C.color "white alt") $ batPercent 30)

-- TopLeft Loggers
myFocusL  =
  lemonCentreAlignL $ lemonColorL (C.color "white") $ shortenL 100 logTitle

myDateL =
  lemonPadLeftL 20 $
  labelL (lemonColor (C.color "white alt") "•") ++!
  lemonPadAroundL 10 (lemonColorL (C.color "white alt") $ date "%e %b %y, %H:%M")

myWindowsL =
  lemonPadLeftL 10 $
  lemonColorL (C.color "white alt") $
 logCmd "bash .xmonad/window_info.sh"
--  logConst "yo"
--  lemonPadLeftL 10 $
--  labelL (lemonColor (C.color "white alt") text)
--  where
--    text = "• " ++ show windowIndex W.index ++ "/" ++ show windowCount


--windowIndex :: Window -> StackSet i l Window s sd -> Maybe Int
--windowIndex w s = elemIndex w $ allWindowsInCurrentWorkspace s


--allWindowsInCurrentWorkspace :: W.StackSet i l a sid sd -> [a]
--allWindowsInCurrentWorkspace ws =
----  W.integrate' . W.stack . W.workspace . W.current $ ws


--windowCount :: Integer
--windowCount = 1
--------------------------------------------------------------------------------------------
-- Bar                                                                                    --
--------------------------------------------------------------------------------------------

data LFlag = LFlag
  { xPosLF       :: Int
  , yPosLF       :: Int
  , widthLF      :: Int
  , heightLF     :: Int
  , clickableLF  :: Int
  , fgColorLF    :: String
  , bgColorLF    :: String
  , fontsLF      :: [String]
  , optionLF     :: String
  }

lemonFlagsToStr :: LFlag -> String
lemonFlagsToStr lf =
  " -g '" ++ show(widthLF lf) ++
  "x" ++ show(heightLF lf) ++
  "+" ++ show(xPosLF lf) ++
  "+" ++ show(yPosLF lf) ++
  "' -a '" ++ show(clickableLF lf) ++
  "' -F'" ++ fgColorLF lf ++
  "' -B'" ++ bgColorLF lf ++
  "' -f '" ++ intercalate "' -f '" (fontsLF lf) ++
  "' " ++ optionLF lf

lemonLeftAlignL :: Logger -> Logger
lemonLeftAlignL = (fmap . fmap) lemonLeftAlign

lemonCentreAlignL :: Logger -> Logger
lemonCentreAlignL = (fmap . fmap) lemonCentreAlign

lemonRightAlignL :: Logger -> Logger
lemonRightAlignL = (fmap . fmap) lemonRightAlign

lemonPadLeftL :: Int -> Logger -> Logger
lemonPadLeftL num = (fmap . fmap) (lemonPadLeft num)

lemonPadAroundL :: Int -> Logger -> Logger
lemonPadAroundL num = (fmap . fmap) (lemonPadAround num)

lemonColorL :: String -> Logger -> Logger
lemonColorL color = (fmap . fmap) (lemonColor color)

lemonFontL :: Int -> Logger -> Logger
lemonFontL num = (fmap . fmap) (lemonFont num)

--------------------------------------------------------------------------------------------
-- HARDCODED LOGGERS (you may need to amend them so that they work on your computer)      --
--------------------------------------------------------------------------------------------

-- Concat two Loggers
(++!) :: Logger -> Logger -> Logger
l1 ++! l2 = (liftA2 . liftA2) (++) l1 l2

-- Label
labelL :: String -> Logger
labelL = return . return

-- Init version for Logger
initL :: Logger -> Logger
initL = (fmap . fmap) initNotNull

-- Concat a list of loggers
concatL :: [Logger] -> Logger
concatL = foldr (++!) (return (return ""))

-- Concat a list of loggers with spaces between them
concatWithSpaceL :: [Logger] -> Logger
concatWithSpaceL = foldr (\ x -> (++!) (x ++! labelL " ")) (return (return ""))

initNotNull :: String -> String
initNotNull [] = "0\n"
initNotNull xs = init xs

tailNotNull :: [String] -> [String]
tailNotNull [] = ["0\n"]
tailNotNull xs = tail xs

-- Convert the content of a file into a Logger
fileToLogger :: (String -> String) -> String -> FilePath -> Logger
fileToLogger f e p = do
  let readWithE f1 e1 p1 = E.catch (f1 . initNotNull <$> readFile p1) ((\_ -> return e1) :: E.SomeException -> IO String)
  str <- liftIO $ readWithE f e p
  return $ return str

-- Battery percent
batPercent :: Int -> Logger
batPercent v = fileToLogger format "N/A" (C.battery "capacity")
  where
    format x = if (read x::Int) <= v then lemonColor (C.color "red") x else lemonColor (C.color "white alt") x

-- Battery status
batStatus :: Logger
batStatus = fileToLogger id "AC Conection" (C.battery "status")

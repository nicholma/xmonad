{-# OPTIONS_GHC -fwarn-unused-imports -fno-warn-name-shadowing #-}
module Bindings ( myKeys
                , myMouseBindings
                ) where

import qualified Data.Map as M

import Graphics.X11.ExtraTypes.XF86

import XMonad

import XMonad.Actions.CycleWS
import XMonad.Actions.GridSelect
import XMonad.Actions.Navigation2D

import XMonad.Layout.BinarySpacePartition

import XMonad.Util.Run
import XMonad.Util.Scratchpad

import qualified XMonad.StackSet as W
import qualified XMonad.Actions.FlexibleResize as Flex

import Themes

myKeys :: XConfig Layout -> M.Map (KeyMask, KeySym) (X ())
myKeys conf @ XConfig { XMonad.modMask = modMask } = M.fromList $
  [ ((modMask, xK_Return), safeSpawnProg $ XMonad.terminal conf)
  , ((modMask .|. shiftMask, xK_Return), safeSpawnProg "terminal-cwd")

  , ((modMask, xK_g), goToSelected $ gsConfig colorizer)

  , ((modMask .|. shiftMask, xK_q), safeSpawnProg "killall lemonbar; xmonad --restart")
  , ((modMask, xK_c), safeSpawnProg "chromium")
  , ((modMask, xK_f), safeSpawnProg "firefox")
  , ((modMask, xK_d), safeSpawnProg "nemo")
  , ((modMask, xK_s), safeSpawnProg "slock")

  , ((modMask.|. shiftMask, xK_c), kill)
  , ((mod1Mask, xK_F4), kill)

  , ((modMask, xK_space), sendMessage NextLayout)
  , ((modMask .|. shiftMask, xK_space ), setLayout $ XMonad.layoutHook conf)
  , ((modMask, xK_n), refresh)

  , ((modMask, xK_Tab), windows W.focusDown)
  , ((modMask .|. shiftMask, xK_Tab), windows W.focusDown)

  , ((modMask, xK_k), windows W.focusUp)
  , ((modMask, xK_j), windows W.focusDown)
  , ((modMask, xK_a), windows W.focusMaster)

  -- Shrink and Expand windows for BinarySpacePartition
  , ((mod1Mask, xK_l), sendMessage $ ExpandTowards R)
  , ((mod1Mask, xK_h), sendMessage $ ExpandTowards L)
  , ((mod1Mask, xK_j), sendMessage $ ExpandTowards D)
  , ((mod1Mask, xK_k), sendMessage $ ExpandTowards U)
  , ((mod1Mask .|. shiftMask , xK_l), sendMessage $ ShrinkFrom R)
  , ((mod1Mask .|. shiftMask , xK_h), sendMessage $ ShrinkFrom L)
  , ((mod1Mask .|. shiftMask , xK_j), sendMessage $ ShrinkFrom D)
  , ((mod1Mask .|. shiftMask , xK_k), sendMessage $ ShrinkFrom U)

  , ((modMask, xK_u), sendMessage Rotate)
  , ((modMask, xK_i), sendMessage Swap)
  , ((modMask, xK_p), sendMessage FocusParent)
  , ((modMask, xK_o), sendMessage SelectNode)
  , ((modMask .|. shiftMask, xK_o), sendMessage MoveNode)
  , ((modMask, xK_t), sendMessage Balance)
  , ((modMask .|. shiftMask, xK_t), sendMessage Equalize)

  , ((modMask .|. shiftMask, xK_a), windows W.swapMaster)
  , ((modMask .|. shiftMask, xK_j), windows W.swapDown)
  , ((modMask .|. shiftMask, xK_k), windows W.swapUp)

  , ((modMask, xK_l), windowGo R False)
  , ((modMask, xK_h), windowGo L False)
  , ((modMask, xK_k), windowGo U False)
  , ((modMask, xK_j), windowGo D False)

  , ((modMask .|. shiftMask, xK_l), windowSwap R False)
  , ((modMask .|. shiftMask, xK_h), windowSwap L False)
  , ((modMask .|. shiftMask, xK_k), windowSwap U False)
  , ((modMask .|. shiftMask, xK_j), windowSwap D False)

  , ((modMask, xK_r), scratchpadSpawnActionCustom "urxvt -name scratchpad -e bash -rcfile .xmonad/scratch.sh")

  , ((modMask .|. shiftMask, xK_Left), sendMessage Shrink)
  , ((modMask .|. shiftMask, xK_Right), sendMessage Expand)

  , ((modMask .|. shiftMask, xK_space), withFocused $ windows . W.sink)

  , ((mod1Mask .|. controlMask, xK_Left),  prevWS)
  , ((modMask, xK_Left), prevWS)
  , ((modMask, xK_Right), nextWS)
  , ((mod1Mask .|. controlMask, xK_Right), nextWS)




  , ((0, xF86XK_MonBrightnessUp), safeSpawnProg "xbacklight -inc 5%")
  , ((0, xF86XK_MonBrightnessDown), safeSpawnProg "xbacklight -dec 5%")
  , ((0, xK_Print), safeSpawnProg "scrot '%Y-%m-%d_$wx$h.png'")

  , ((0, xF86XK_AudioMute), spawn "amixer set Master toggle")
  , ((0, xF86XK_AudioRaiseVolume), spawn "amixer set Master 2%+")
  , ((0, xF86XK_AudioLowerVolume), spawn "amixer set Master 2%-")

  , ((modMask, xF86XK_AudioRaiseVolume), spawn "pactl set-sink-volume 0 +1.5%")
  , ((modMask, xF86XK_AudioLowerVolume), spawn "pactl set-sink-volume 0 -1.5%")
  , ((modMask, xF86XK_AudioMute), spawn "pactl set-sink-mute 0 toggle")

  , ((0, xF86XK_AudioPlay), safeSpawnProg "mpc toggle")
  , ((0, xF86XK_AudioNext), safeSpawnProg "ncmpcpp next")
  , ((0, xF86XK_AudioPrev), safeSpawnProg "ncmpcpp prev")
  ] ++
  --Switch to n workspaces and send client to n workspaces
  [((m .|. modMask, k), windows $ f i)
    | (i, k) <- zip (XMonad.workspaces conf) ([xK_1 .. xK_9] ++ [xK_0])
    , (f, m) <- [(W.greedyView, 0), (W.shift, shiftMask)]
  ]
  where
    prevWS = moveTo Prev (WSIs (return $ (/= "NSP") . W.tag))
    nextWS = moveTo Next (WSIs (return $ (/= "NSP") . W.tag))

-- Mouse bindings
myMouseBindings :: XConfig Layout -> M.Map (KeyMask, Button) (Window -> X ())
myMouseBindings XConfig {XMonad.modMask = modMask} = M.fromList
  [ ((modMask, button1), \w -> focus w >> mouseMoveWindow w >> windows W.shiftMaster)
  , ((modMask, button2), \w -> focus w >> windows W.shiftMaster)
  , ((modMask, button3), \w -> focus w >> Flex.mouseResizeWindow w)
  , ((modMask, button4), const prevWS)
  , ((modMask, button5), const nextWS)
  , ((modMask .|. shiftMask, button4), const shiftToPrev)
  , ((modMask .|. shiftMask, button5), const shiftToNext)
  ]
  where
    prevWS = moveTo Prev (WSIs (return $ (/="NSP") . W.tag))
    nextWS = moveTo Next (WSIs (return $ (/="NSP") . W.tag))
    shiftToPrev = shiftTo Prev (WSIs (return $ (/="NSP") . W.tag))
    shiftToNext = shiftTo Next (WSIs (return $ (/="NSP") . W.tag))

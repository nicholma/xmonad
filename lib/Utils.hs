{-# OPTIONS_GHC -fwarn-unused-imports #-}
module Utils ( getScreenRes
             , Res
             , xRes
             , yRes) where

-- Modules
import XMonad
import Graphics.X11.Xinerama


getScreenRes :: String -> Int -> IO Res
getScreenRes d n = do
  dpy <- openDisplay d
  r <- liftIO $ getScreenInfo dpy
  closeDisplay dpy
  return Res
    { xRes = fromIntegral $ rect_width $ r !! n
    , yRes = fromIntegral $ rect_height $ r !! n
    }

-- Screen Resolution
data Res = Res
  { xRes :: Int
  , yRes :: Int
  }

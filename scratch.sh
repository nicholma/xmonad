
#reverse_command() {
#  if (( ${#BASH_SOURCE[@]} <= 1 ));
#  then
#    eval "launch ${BASH_COMMAND}";
#    false;
#  else
#    true;
#  fi;
#}
##shopt -s extdebug
#trap reverse_command DEBUG

. "${HOME}/.bashrc"
export HISTFILE="${HOME}/.prompt_history"
bind 'RETURN: "\e[1~launch \e[4~;xdotool key super+r;reset \n"'

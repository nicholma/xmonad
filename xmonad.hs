{-# LANGUAGE NoMonomorphismRestriction, MultiParamTypeClasses, FlexibleContexts, FlexibleInstances #-}
{-# OPTIONS_GHC -fwarn-unused-imports #-}

-- Modules
import XMonad
import XMonad.Config.Desktop
import XMonad.Layout.BinarySpacePartition
import XMonad.Layout.NoBorders
import XMonad.Layout.Minimize
import XMonad.Layout.Spacing
import XMonad.Hooks.CurrentWorkspaceOnTop
import XMonad.Hooks.DynamicHooks
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.SetWMName
import XMonad.Hooks.ManageHelpers
import XMonad.Util.Timer
import XMonad.Util.Cursor
import XMonad.Util.Scratchpad

import XMonad.Actions.Navigation2D
import Data.Monoid
import qualified XMonad.StackSet as W
import qualified XMonad.Util.ExtensibleState as XS

import Bindings
import Bar
import Utils
import qualified Config as C

main :: IO ()
main = do
  r <- getScreenRes ":0" 0  --display ":0", screen 0
  lemonBar <- lemonSpawnPipe $ lemonFlags r
  xmonad $ withNavigation2DConfig myNavigation2DConf $ desktopConfig
    { terminal              = C.myTerminal
    , modMask               = mod4Mask
    , focusFollowsMouse     = True
    , clickJustFocuses      = True
    , borderWidth           = 0
    , normalBorderColor     = C.color "black"
    , focusedBorderColor    = C.color "blue"
    , workspaces            = C.myWorkspaces
    , startupHook           = myStartupHook
    , handleEventHook       = myHandleEventHook
    , layoutHook            = myLayoutHook
    , manageHook            = myManageHook
    , logHook               =
      lemonLogHook lemonBar   <+>
      currentWorkspaceOnTop   <+>
      ewmhDesktopsLogHook     <+>
      setWMName "LG3D"
    , keys                  = myKeys
    , mouseBindings         = myMouseBindings
    }

-- Startup Hook
myStartupHook =
  ewmhDesktopsStartup <+>
  setDefaultCursor xC_left_ptr <+>
  (startTimer 1 >>= XS.put . TID)

newtype TidState = TID TimerId deriving Typeable

instance ExtensionClass TidState where
  initialValue = TID 0

-- Handle event hook
myHandleEventHook =
  -- fullscreenEventHook <+>
  ewmhDesktopsEventHook <+>
  clockEventHook <+>
  handleEventHook desktopConfig
  where
    clockEventHook e = do             --thanks to DarthFennec
      (TID t) <- XS.get               --get the recent Timer id
      _ <- handleTimer t e $ do            --run the following if e matches the id
        startTimer 1 >>= XS.put . TID --restart the timer, store the new id
        ask >>= logHook . config      --get the loghook and run it
        return Nothing                --return required type
      return $ All True               --return required type

-- Layout hook
myLayoutHook =
  allLayouts where
    allLayouts  =
      myBSP |||
      myTabd |||
      myFull
    myTabd = avoidStruts $ spacingRaw False (Border 6 6 6 6) True (Border 6 6 6 6) True $ Full
    myBSP = avoidStruts $ spacingRaw False (Border 6 6 6 6) True (Border 6 6 6 6) True $ minimize emptyBSP
    myFull = noBorders Full

-- Manage Hook
myManageHook :: ManageHook
myManageHook =
  manageDocks <+>
  scratchpadManageHook (W.RationalRect 0.05 0.5 0.9 0.3) <+>
  dynamicMasterHook <+>
  manageWindows <+>
  manageHook desktopConfig

-- Manage Windows
manageWindows :: ManageHook
manageWindows = composeAll . concat $

  , [ className =? c --> doIgnore                      | c <- myIgnoreCC ]
  , [ className =? c --> doShift (C.myWorkspaces !! 1) | c <- myWebS     ]
  , [ className =? c --> doShift (C.myWorkspaces !! 2) | c <- myCodeS    ]
  , [ className =? c --> doShift (C.myWorkspaces !! 3) | c <- myGfxS     ]
  , [ className =? c --> doShift (C.myWorkspaces !! 4) | c <- myChatS    ]
  , [ className =? c --> doShift (C.myWorkspaces !! 9) | c <- myAlt3S    ]
  , [ className =? c --> doCenterFloat                 | c <- myFloatCC  ]
  , [ name      =? n --> doIgnore                      | c <- myIgnoreNM ]
  , [ name      =? n --> doCenterFloat                 | n <- myFloatCN  ]
  , [ name      =? n --> doSideFloat NW                | n <- myFloatNW  ]
  , [ name      =? n --> doSideFloat SE                | n <- myFloatSE  ]
  , [ className =? c --> doF W.focusDown               | c <- myFocusDC  ]
  , [ isDialog --> doCenterFloat ]
  , [ isFullscreen   --> doFullFloat ]
  ] where
    name       = stringProperty "WM_NAME"
    myIgnores  = ["desktop", "desktop_window"]
    myIgnoreCC = ["Dunst", "deepin-notifications"]
    myIgnoreNM = ["zoom"]
    myWebS     = ["Firefox", "firefox"]
    myCodeS    = ["NetBeans IDE 7.3"]
    myChatS    = ["Pidgin", "Xchat"]
    myGfxS     = ["Gimp", "gimp", "GIMP"]
    myAlt3S    = ["Chromium", "Amule", "Transmission-gtk"]
    myFloatCC  = ["MPlayer", "mplayer2", "File-roller", "zsnes", "Gcalctool", "Exo-helper-1"
                 , "Gksu", "Galculator", "Nvidia-settings", "XFontSel", "XCalc", "XClock"
                 , "Ossxmix", "Xvidcap", "Main", "Wicd-client.py"]
    myFloatCN  = ["Choose a file", "Open Image", "File Operation Progress", "Firefox Preferences"
                 , "Preferences", "Search Engines", "Set up sync", "Passwords and Exceptions"
                 , "Autofill Options", "Rename File", "Copying files", "Moving files"
                 , "File Properties", "Replace", "Quit GIMP", "Change Foreground Color"
                 , "Change Background Color","Add Torrents"]
    myFloatNW  = ["Event Tester"]
    myFloatSE  = ["Microsoft Teams Notification"]
    myFocusDC  = ["Event Tester", "Notify-osd"]


myNavigation2DConf = def
  { defaultTiledNavigation= centerNavigation
  , floatNavigation = centerNavigation
  , screenNavigation = lineNavigation
  }

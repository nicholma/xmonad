{-# LANGUAGE FlexibleInstances #-}
{-# OPTIONS_GHC -fwarn-unused-imports -fno-warn-type-defaults #-}
module Config ( myWorkspaces
              , battery
              , myTerminal
              , color
              , aaFont
              , fixedFont
              , smallFont
              ) where

import XMonad.Config.Prime


myWorkspaces :: [WorkspaceId]
myWorkspaces = map show $ [1..9] ++ [0]

battery :: String -> String
battery "capacity" = "/sys/class/power_supply/BAT0/capacity"
battery "status" = "/sys/class/power_supply/BAT0/status"
battery _ = ""

myTerminal :: String
myTerminal = "alacritty"

aaFont :: String
aaFont = "Cousine-10"

fixedFont :: String
fixedFont = "-*-fixed-medium-r-*-*-*-140-*-*-*-*-*-*"

smallFont :: String
smallFont = "-*-fixed-medium-r-*-*-*-120-*-*-*-*-*-*"

color :: String -> String
color "bar"       = "#002B2D37"
color "black"     = "#2f343f"
color "black alt" = "#2B2D37"
color "gray"      = "#404552"
color "gray alt"  = "#414857"
color "white"     = "#FCFCFC"
color "white alt" = "#AFB8C6"
color "magenta"   = "#EE78FB"
color "blue"      = "#5294E2"
color "blue alt"  = "#12407C"
color "cyan"      = "#04C6CD"
color "cyan alt"  = "#6FC3C5"
color "red"       = "#FD5879"
color "green"     = "#07DA69"
color "yellow"    = "#F7EA30"
color _           = "#D21994"
